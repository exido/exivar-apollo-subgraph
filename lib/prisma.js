import { PrismaClient } from '@prisma/client';

export default new PrismaClient({
  log: ['query', 'info', `warn`, `error`],
});

// const prisma = new PrismaClient({
//   log: [
//     {
//       emit: 'event',
//       level: 'query',
//     },
//     {
//       emit: 'stdout',
//       level: 'error',
//     },
//     {
//       emit: 'stdout',
//       level: 'info',
//     },
//     {
//       emit: 'stdout',
//       level: 'warn',
//     },
//   ],
// });

// prisma.$on('query', e => {
//   console.log("Query: " + e.query)
//   console.log("Param: " + e.params)
// })

// export default prisma;
