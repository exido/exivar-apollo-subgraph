# Hola Accounts

Steps to launch on local dev

1. Install dependencies

```properties
npm install
```

2. Connect to Vercel and pull down enviroment variables

```properties
vercel env pull
```

3. Make sure you have postgres installed locally and have `UUID` exension installed if not install using

```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

4. run `db push`

```properties
npx prisma db push
```

5. generate prisma client

```properties
npx prisma generate
```

---

6. to generate api key to be passed to gateway,

```properties
cd generator && yarn gen:api [provider<default:localhost.accounts>] <optional>[key]
```

console outputs the `KEY` you need for gateway.

7. Start Redis server

```properties
redis
```

8. Set Required Enviroment variables

### for subgraph to work properly you need to pass the followings throw headers coming from gateway

- `x-accounts-api-key`
- `x-accounts-api-provider`
- `x-user`
- `x-jwt-token`
