import { randomBytes, pbkdf2Sync } from 'crypto';
import error from '../utils/errors';
import prisma from '../lib/prisma';
import client, { minutesToExpire } from './redis';
import { asyncTry } from './asynctry';

const REDIS_KEY = `${process.env.NAMESPACE}_key`;

export const generateSalt = () => randomBytes(32).toString('hex');

export const generatePasswordHash = (password, salt) => pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');

export const verifyHash = (password, salt, hash) => {
  if (!password) throw error(404, 'No Password provided');
  if (!salt) throw error(404, 'No salt provided');
  return generatePasswordHash(password, salt) === hash;
};

export const verifyAPI = async (provider, key) => {
  const [cacheKey, cacheKeyError] = await asyncTry(client.get(REDIS_KEY));
  if (cacheKey) {
    const [providerName, salt, hash] = cacheKey.split(',');
    if (verifyHash(key, salt, hash) && providerName === provider) return true;
  }
  const [remote, remoteError] = await asyncTry(
    prisma.apiToken.findUnique({
      where: {
        provider,
      },
      select: {
        salt: true,
        hash: true,
      },
    })
  );
  if (remoteError || !remote) {
    throw error(400, 'api key not auth');
  }
  if (verifyHash(key, remote.salt, remote.hash)) {
    try {
      //set key for 2 hours
      await client.setex(REDIS_KEY, minutesToExpire(120), `${provider},${remote.salt},${remote.hash}`);
    } catch (error) {
      console.log(error);
    }
    return true;
  }
  throw error(400, 'api key not auth');
};
