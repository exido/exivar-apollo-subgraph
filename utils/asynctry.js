export const asyncTry = async (prom) => {
  try {
    const data = await prom;
    return [data, null];
  } catch (error) {
    console.log(error);
    return [null, error];
  }
};
export const syncTry = (fn) => {
  try {
    const data = fn;
    return [data, null];
  } catch (error) {
    console.log(error);
    return [null, error];
  }
};
