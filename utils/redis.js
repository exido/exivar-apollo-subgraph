import Redis from 'ioredis';
export default new Redis(process.env.REDIS_URL);
export const minutesToExpire = (minutes = 60) => minutes * 60;
