export default function error(code, message) {
  return {
    code,
    message,
  };
}

export const ERRORS_LIST = {
  user: {
    exists_400: 'Account with this email address already exists, try logging in instead',
    signup_failed_400: 'Signup Failed, Please try again.',
    user_not_found_404: 'No account with this email was found, try registering instead',
    login_invalid_400: 'Invalid email and password combination',
  },
  server: {
    internal_505: 'Internal Error, Contact Suppoert.',
  },
};
