import permissions from '../apollo/permissions';
import schema from '../apollo/schema';
import { verifyAPI } from '../utils/auth';
import { ApolloServer } from 'apollo-server-micro';
import { applyMiddleware } from 'graphql-middleware';

const apolloServer = new ApolloServer({
  schema: applyMiddleware(schema, permissions),
  context(ctx) {
    ctx.providerId = ctx.req?.headers?.[`x-${process.env.NAMESPACE}-api-provider`];
    ctx.jwtSecret = ctx.req?.headers?.['x-jwt-token'];
    ctx.user = ctx.req?.headers?.['x-user'] ? JSON.parse(ctx.req?.headers?.['x-user']) : null;
    if (ctx.user) {
      ctx.user.can = (permission) => ctx.user?.permissions?.includes(permission);
      ctx.user.is = (role) => user?.role === role;
    }
    return ctx;
  },
});

const startServer = apolloServer.start();

export default async function handler(req, res) {
  try {
    // ex. (localhost, 33mmjj)
    await verifyAPI(`${req.headers?.[`x-${process.env.NAMESPACE}-api-provider`]}`, req.headers?.[`x-${process.env.NAMESPACE}-api-key`]);
    await startServer;
    await apolloServer.createHandler({
      path: '/api/v1',
    })(req, res);
  } catch (error) {
    res.status(400).send(error);
  }
}

export const config = {
  api: {
    bodyParser: false,
  },
};
