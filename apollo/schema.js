import { buildFederatedSchema } from '@apollo/federation';

import typeDefs from './type-defs';
import resolvers from './resolvers';

export default buildFederatedSchema([
  {
    typeDefs,
    resolvers,
  },
]);
