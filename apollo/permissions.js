import { allow, deny, rule, shield } from 'graphql-shield';

const isAuthenticated = rule()((_, __, { user }) => user !== null);
const can = (permission) => rule()((_, __, { user }) => isAuthenticated && user.can(permission));
const is = (role) => rule()((_, __, { user }) => isAuthenticated && user.is(role));
const hasProvider = rule()((_, __, { provider }) => provider !== null);

const permissions = shield({});

export default permissions;
