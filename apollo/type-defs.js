import { gql } from 'apollo-server-micro';

export default gql`
  type Product {
    id: String
  }

  extend type Query {
    product: Product
  }
`;
