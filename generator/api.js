#! /usr/bin/env node

const { PrismaClient } = require('@prisma/client');
const { randomBytes, pbkdf2Sync } = require('crypto');

const generateSalt = (length = 32) => randomBytes(length).toString('hex');
const generatePasswordHash = (password, salt) => pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');

const prisma = new PrismaClient({
  log: ['query', 'info', `warn`, `error`],
});

const args = process.argv.slice(2, process.argv.length);

async function main(provider = 'localhost', key = generateSalt(16)) {
  console.info('Generating API KEY');
  if (!key) {
    console.error('error: %S', 'Key is not porivded');
    process.exit(1);
  }
  if (!provider) {
    console.error('error: %S', 'Provider is not porivded');
    process.exit(1);
  }

  const salt = generateSalt();
  const hash = generatePasswordHash(key, salt);
  const dbAPIkey = await prisma.apiToken.create({
    data: {
      hash,
      salt,
      provider,
    },
  });
  console.log('🔐 Generated key with: ', key);
}

main(args[0], args[1])
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
